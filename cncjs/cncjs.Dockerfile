FROM balenalib/rpi-debian-node:jessie

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get dist-upgrade -y

RUN apt-get install build-essential git -y
RUN apt-get install htop iotop nmon lsof screen -y
# RUN apt-get install npm nodejs -y

RUN npm install npm@latest -g
RUN npm install -g cncjs --unsafe-perm

CMD cncjs --port 80
